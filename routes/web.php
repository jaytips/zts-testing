<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('zts-home');

Auth::routes();

Route::get('/shop-home', 'HomeController@index')->name('shop-home');

//admin
Route::get('/zts-admin',function(){
    return view('admin');
});
